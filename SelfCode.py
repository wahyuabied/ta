import numpy as np
import cv2
import math


#Kernel
#kernel_size_ = ukuran matrix 
#sigma = bentuk elips dari envelope gaussian dari filter gabor (bisa sama ataupun tidak)
#psi = 0 ()


def build_filters(lambdas, ksize, gammaSigmaPsi):
	filters = []
	thetas = []
    # Thetas 1
	thetas.extend([0,30,45,60,90,120,135,150])
	# thetas.extend([0, 45, 90, 135])
	thetasInRadians = [np.deg2rad(x) for x in thetas]
	for lamb in lambdas:
		for theta in thetasInRadians:
			params = {'ksize': (ksize, ksize), 'sigma': gammaSigmaPsi[1], 'theta': theta, 'lambd': lamb,'gamma':gammaSigmaPsi[0], 'psi': gammaSigmaPsi[2], 'ktype': cv2.CV_64F}
			kern = cv2.getGaborKernel(**params)
			# cv2.imshow('kern'+str(lamb)+str(theta), kern)
			kern /= 1.5 * kern.sum()
			filters.append((kern, params))
	return filters

#Gabor
def getLambdaValues(img):
	height, width = img.shape
    #calculate radial frequencies.
	max = (width/4) * math.sqrt(2)
	min = 4 * math.sqrt(2)
	temp = min
	radialFrequencies = []
    # Lambda 1
    # -------------------------------------
	while(temp < max):
		radialFrequencies.append(temp)
		temp = temp * 2
    # Lambda 2
    # -------------------------------------
    # while(temp < max):
    #     radialFrequencies.append(temp)
    #     temp = temp * 1.5
	radialFrequencies.append(max)
	lambdaVals = []
	for freq in radialFrequencies:
		print(width/freq)
		lambdaVals.append(width/freq)
	return lambdaVals

def getFilterImages(filters, img):
	featureImages = []
	for i in np.arange(0, len(filters),1):
		kern, params = filters[i]
		fimg = cv2.filter2D(img, cv2.CV_8UC3, kern)
		cv2.imshow('imageReal'+str(i), fimg)
		featureImages.append(fimg)
	return featureImages


if __name__ == '__main__': 

	##set data to database
	pathsEarly = 'early/'
	pathsLate = 'late/'
	pathsSehat = 'sehat/'


	#scale  = [0.06,0.09,0.13,0.18,0.25] # frequency
	scale =[0.1]
	gammaSigmaPsi = [0.5,4,0]
	for x in range(1,2):
		img = cv2.imread(pathsEarly+'('+str(x)+').jpg',0)
		lambdas = getLambdaValues(img)
		cv2.imshow('imageReal', img)
		kernel = build_filters(lambdas,31,gammaSigmaPsi)
		getFilterImages(kernel,img)

	cv2.waitKey(0)
	cv2.destroyAllWindows()
